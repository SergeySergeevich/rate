package com.enin.rate;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.net.URL;
import java.net.URLConnection;


public class UpdateTask extends AsyncTask<String, Void, Void> {
    private static UpdateTask myTask = null;
    private Context cntx = null;
    private DBAdapter tdb;
    private double usd = 0, eur = 0;
    private String date = "";
    private boolean isOK = false;

    private UpdateTask(DBAdapter db, Context ctx) {
        super();
        tdb = db;
        myTask = this;
        cntx = ctx;
    }


    public static UpdateTask getUpdateTask(DBAdapter db, Context ctx) {
        if (myTask == null) {
            return new UpdateTask(db, ctx);
        } else return null;
    }

    @Override
    protected Void doInBackground(String... params) {
        if (MyWidget.isOnline(cntx))
            try {
                date = params[1];
                URL url = new URL(params[0] + params[1]);
                Log.d("myLogs", params[0] + params[1]);
                URLConnection conn = url.openConnection();
                Log.d("myLogs", "openConnection()");
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.parse(conn.getInputStream());
                Log.d("myLogs", "parse()");
                NodeList nodes = doc.getElementsByTagName("ValCurs");
                Log.d("myLogs", "nodes.getLength()" + Integer.toString(nodes.getLength()));
                Element valutes = (Element) nodes.item(0);
                NodeList Lvalutes = valutes.getElementsByTagName("Valute");
                Log.d("myLogs", "Lvalutes.getLength()" + Integer.toString(Lvalutes.getLength()));
                for (int i = 0; i < Lvalutes.getLength(); i++) {
                    Element element = (Element) Lvalutes.item(i);
                    NodeList CharCode = element.getElementsByTagName("CharCode");
                    Element line = (Element) CharCode.item(0);
                    String CodeS = line.getTextContent();
                    Log.d("myLogs", CodeS);
                    if (CodeS.equals("USD")) {
                        Log.d("myLogs", "USD");
                        NodeList valueT = element.getElementsByTagName("Value");
                        Element value = (Element) valueT.item(0);
                        usd = Double.parseDouble(replace(value.getTextContent(), ',', '.'));
                        Log.d("myLogs", "USD " + usd);
                    }
                    if (CodeS.equals("EUR")) {
                        NodeList valueT = element.getElementsByTagName("Value");
                        Element value = (Element) valueT.item(0);
                        eur = Double.parseDouble(replace(value.getTextContent(), ',', '.'));
                        Log.d("myLogs", "eurr() " + eur);
                    }
                }
                tdb.open();
                Log.d(AppClass.log, "eur() " + Double.toString(eur) + "usd() " + Double.toString(usd) + date);
                tdb.insertRate(usd, eur, date);
                tdb.close();
                isOK = true;
                this.cancel(true);
                Log.d(AppClass.log, "ALLGOOD");

            } catch (Exception e) {
                e.printStackTrace();
                isOK = false;
                this.cancel(true);
                Log.d(AppClass.log, "Error was");
            }
        else {
            isOK = false;
            this.cancel(true);
            Log.d(AppClass.log, "HaveNoInternet");
        }

        return null;
    }

    protected void onCancelled() {
        super.onCancelled();
        Intent intent = new Intent();
        intent.setAction("com.enin.rate.DATABASEUPDATE");
        intent.putExtra("isOk", isOK);
        cntx.sendBroadcast(intent);
        Log.d("myLogs", "update END");
        myTask = null;
    }

    public String replace(String str, char WhatReplace, char OnWhatReplace) {
        if (str == null)
            return str;
        else {
            char[] chars = str.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                if (chars[i] == WhatReplace) {
                    chars[i] = OnWhatReplace;
                    return String.valueOf(chars);
                }
            }
            return str;
        }
    }


}
