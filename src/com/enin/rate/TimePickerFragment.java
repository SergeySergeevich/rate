package com.enin.rate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.TimePicker;

/**
 * Created by Сергей on 30.09.2014.
 */
public class TimePickerFragment extends DialogFragment {
    public static final String EXTRA_DATE_HOUR =
            "com.enin.rate.TimeFragment.time.hour";
    public static final String EXTRA_DATE_MIN =
            "com.enin.rate.TimeFragment.time.min";
    int hour = 0;
    int min = 0;

    public static TimePickerFragment newInstance(int hh, int mm) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_DATE_HOUR, hh);
        args.putInt(EXTRA_DATE_MIN, mm);

        TimePickerFragment fragment = new TimePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        hour = getArguments().getInt(EXTRA_DATE_HOUR, 1);
        min = getArguments().getInt(EXTRA_DATE_MIN, 0);
        View v = getActivity().getLayoutInflater().inflate(R.layout.timepicker, null);
        TimePicker timePicker = (TimePicker) v.findViewById(R.id.dialog_time_timepicker);
        timePicker.setIs24HourView(true);
        timePicker.setCurrentHour(hour);
        timePicker.setCurrentMinute(min);
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                getArguments().putInt(EXTRA_DATE_HOUR, hour);
                getArguments().putInt(EXTRA_DATE_MIN, min);
            }
        });


        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle(R.string.setDate)
                .setPositiveButton("Подтвердить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendResult(Activity.RESULT_OK);
                    }
                })
                .create();


    }

    private void sendResult(int resultCode) {
        if (getTargetFragment() == null) return;

        Intent i = new Intent();
        i.putExtra(EXTRA_DATE_HOUR, hour);
        i.putExtra(EXTRA_DATE_MIN, min);

        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, i);

    }
}
