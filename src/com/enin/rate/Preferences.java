package com.enin.rate;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Сергей on 17.09.2014.
 */
public class Preferences extends Fragment {
    private static final String DIALOG_TIME = "time";
    TextView timeUptext;
    Button changeTime;
    int hour;
    int min;
    int REQUEST_TIME = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(AppClass.log, "Fragment onCreateView");
        View view = inflater.inflate(R.layout.pref_lay, null);
        timeUptext = (TextView) view.findViewById(R.id.TextTimeUpdate);
        changeTime = (Button) view.findViewById(R.id.ChengeTimeUpdate);
        SharedPreferences myPref = getActivity().getApplicationContext().getSharedPreferences(AppClass.PREF, Activity.MODE_PRIVATE);
        hour = myPref.getInt("Hour", 1);
        min = myPref.getInt("Min", 0);
        changeTime.setText(Integer.toString(hour) + ":" + Integer.toString(min));
        changeTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                TimePickerFragment dialog = TimePickerFragment.newInstance(hour, min);
                dialog.setTargetFragment(Preferences.this, 0);
                dialog.show(fm, DIALOG_TIME);
            }
        });

        return view;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent time) {
        if (requestCode != Activity.RESULT_OK) return;
        if (requestCode == REQUEST_TIME) {
            hour = time.getIntExtra(TimePickerFragment.EXTRA_DATE_HOUR, 1);
            min = time.getIntExtra(TimePickerFragment.EXTRA_DATE_MIN, 0);
            changeTime.setText(Integer.toString(hour) + ":" + Integer.toString(min));
            SharedPreferences myPref = getActivity().getApplicationContext().getSharedPreferences(AppClass.PREF, Activity.MODE_PRIVATE);
            SharedPreferences.Editor ed = myPref.edit();
            ed.putInt("Hour", hour);
            ed.putInt("Min", min);
        }

    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(AppClass.log, "Fragment1 onAttach");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(AppClass.log, "Fragment1 onCreate");
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(AppClass.log, "Fragment onActivityCreated");
    }

    public void onStart() {
        super.onStart();
        Log.d(AppClass.log, "Fragment onStart");
    }

    public void onResume() {
        super.onResume();
        Log.d(AppClass.log, "Fragment onResume");
    }

    public void onPause() {
        super.onPause();
        Log.d(AppClass.log, "Fragment onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d(AppClass.log, "Fragment onStop");
    }

    public void onDestroyView() {
        super.onDestroyView();
        Log.d(AppClass.log, "Fragment onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(AppClass.log, "Fragment onDestroy");
    }

    public void onDetach() {
        super.onDetach();
        Log.d(AppClass.log, "Fragment onDetach");
    }


}
