package com.enin.rate;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * Created by Сергей on 11.09.2014.
 */
public class ListOfRates extends ListFragment {
    ArrayList<rate> Rates;
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(AppClass.log, "onActivityCreated()");
        DBAdapter db = new DBAdapter(getActivity());
        Rates = new ArrayList<rate>();
        db.open();
        Cursor c = null;
        try {
            c = db.getAllRows();
        } catch (Exception E) {
            Log.d(AppClass.log, "getRowALL() fail");
        }
        if (c != null && c.moveToFirst()) {
            for (int i = 0; i < c.getCount(); i++) {
                Rates.add(new rate(c.getDouble(c.getColumnIndex("dollar")), c.getDouble(c.getColumnIndex("euro")), c.getString(c.getColumnIndex("date")), c.getInt(c.getColumnIndex("_id"))));
                Log.d(AppClass.log, "Getting rates №");
                c.moveToNext();
            }

            Log.d(AppClass.log, "CreateAdapter");
            RateAdapter adapter = new RateAdapter(getActivity(), Rates);
            Log.d(AppClass.log, "CreateAdapter OK");
            setListAdapter(adapter);
            Log.d(AppClass.log, "SetAdapter OK");

        } else Log.d(AppClass.log, "SetAdapter FAILL");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fraglist, null);
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Toast.makeText(getActivity(), "position = " + position, Toast.LENGTH_SHORT).show();
    }


}