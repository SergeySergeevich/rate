package com.enin.rate;


import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.RemoteViews;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyWidget extends AppWidgetProvider {
    static final String LayAdd = "http://www.cbr.ru/scripts/XML_daily.asp?date_req=";

    static boolean isOnline(Context xnt) {
        ConnectivityManager cm =
                (ConnectivityManager) xnt.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appwm, int[] appWidgetIds) {
        super.onUpdate(context, appwm, appWidgetIds);

        SharedPreferences myPref = context.getSharedPreferences(AppClass.PREF, Activity.MODE_PRIVATE);
        Log.d(AppClass.log, "onUpdate()");
        String currentDateTimeString = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        DBAdapter db = new DBAdapter(context);
        Log.d(AppClass.log, "db adapter");
        db.open();
        Log.d(AppClass.log, "open()");
        Cursor c = null;
        try {
            c = db.getRowOnDate(currentDateTimeString);
        } catch (Exception E) {
            Log.d(AppClass.log, "getRowOnDate() fail");
        }

        if (c != null && c.moveToFirst()) {
            Log.d(AppClass.log, "NowRATE");
            for (int i : appWidgetIds) {
                Log.d(AppClass.log, "Updating widgets");
                updateWidget(context, appwm, i, new rate(c.getDouble(c.getColumnIndex("dollar")), c.getDouble(c.getColumnIndex("euro")), c.getString(c.getColumnIndex("date")), c.getInt(c.getColumnIndex("_id"))));
            }
            SharedPreferences.Editor edit = myPref.edit();
            edit.putBoolean("isUpdated", true);
            edit.commit();
        } else {
            Log.d(AppClass.log, "Updating DB");
            UpdateTask myUpdate = UpdateTask.getUpdateTask(db, context);
            if (myUpdate != null) {
                Log.d(AppClass.log, "Getting task() Ok");
                myUpdate.execute(LayAdd, currentDateTimeString);
                Log.d(AppClass.log, "task run()");
            } else {
                Log.d(AppClass.log, "I think task already go");
            }
        }
        db.close();
    }

    public void LetsCallOnUpdate(Context context) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int[] appWidgetIds =
                appWidgetManager.getAppWidgetIds(new ComponentName(context, this.getClass()));
        onUpdate(context, appWidgetManager, appWidgetIds);

    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        Log.d(AppClass.log, "onReceive() " + intent.getAction());
        if ("com.enin.rate.DATABASEUPDATE".equals(intent.getAction())) // Получен интент от UpdateTask
        {
            if (intent.getBooleanExtra("isOk", false)) {
                LetsCallOnUpdate(context);
            } else {
                SharedPreferences myPref = context.getSharedPreferences(AppClass.PREF, Activity.MODE_PRIVATE);
                SharedPreferences.Editor edit = myPref.edit();
                edit.putBoolean("isUpdated", false);
                edit.commit();
            }
        }
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            if (isOnline(context)) {
                SharedPreferences myPref = context.getSharedPreferences(AppClass.PREF, Activity.MODE_PRIVATE);
                if (!myPref.getBoolean("isUpdated", false)) {
                    LetsCallOnUpdate(context);
                }
            }
        }
        if ("com.enin.rate.ALARM".equals(intent.getAction())) {
            LetsCallOnUpdate(context);
            AppClass.getInstance().setAlarm();
        }
    }

    public void updateWidget(Context context, AppWidgetManager appwm, int id, rate myRate) {
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
        if (myRate != null) {
            views.setTextViewText(R.id.dateToday, myRate.getDate());
            views.setTextViewText(R.id.Dollar, Double.toString(myRate.getDollarrate()));
            views.setTextViewText(R.id.Euro, Double.toString(myRate.getEurorate()));
        } else {
            views.setTextViewText(R.id.dateToday, new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
            views.setTextViewText(R.id.Dollar, "");
            views.setTextViewText(R.id.Euro, "");
        }

        Intent configIntent = new Intent(context, MainActivity.class);
        configIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
        configIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id);
        PendingIntent pIntent = PendingIntent.getActivity(context, id,
                configIntent, 0);
        views.setOnClickPendingIntent(R.id.MainLinLAy, pIntent);
            /*Intent intent = new Intent(context,MyWidget.class);
			intent.setAction("com.enin.rate.TAP");
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, id, intent, PendingIntent.FLAG_CANCEL_CURRENT );
			views.setOnClickPendingIntent(R.layout.widget_layout, pendingIntent );*/
        appwm.updateAppWidget(id, views);
    }
}
