package com.enin.rate;

public class rate {


    private double dollarrate;
    private double eurorate;
    private String date;
    private int id = 0;

    public rate(double us, double eu, String dt, int id) {
        dollarrate = us;
        eurorate = eu;
        date = dt;
        this.id = id;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public double getDollarrate() {
        return round(dollarrate, 2);
    }

    public void setDollarrate(double dollarrate) {
        this.dollarrate = dollarrate;
    }

    public double getEurorate() {
        return round(eurorate, 2);
    }

    public void setEurorate(double eurorate) {
        this.eurorate = eurorate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
