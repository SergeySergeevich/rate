package com.enin.rate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class RateAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<rate> objects;

    public RateAdapter(Context context, ArrayList<rate> records) {
        ctx = context;
        objects = records;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return objects.size();

    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return objects.get(position);

    }

    @Override
    public long getItemId(int position) {
        return objects.get(position).getId();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item_rate, parent, false);
        }
        rate p = getRate(position);
        ((TextView) view.findViewById(R.id.text_date)).setText(p.getDate());
        ((TextView) view.findViewById(R.id.rate_dollar)).setText(Double.toString(p.getDollarrate()));
        ((TextView) view.findViewById(R.id.rate_euro)).setText(Double.toString(p.getEurorate()));
        // добавить стрелочки !!!
        return view;

    }

    rate getRate(int position) {
        return ((rate) getItem(position));
    }
};
