package com.enin.rate;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBAdapter {
    public static final String KEY_ID = "_id";
    public static final String KEY_DOLLAR = "dollar";
    public static final String KEY_EURO = "euro";
    public static final String KEY_DATE = "date";

    private static final String DATABASE_NAME = "dateofrate.db";
    private static final int DATEBASE_VERSION = 1;
    private static final String TABLE_NAME = "rateofdollareuro";

    static final String DATABASE_CREATE = "create table " + TABLE_NAME + "("
            + KEY_ID + " integer primary key autoincrement, "
            + KEY_DOLLAR + " double, "
            + KEY_EURO + " double, " + KEY_DATE + " text not null );";
    final Context context;

    DatabaseHelper DBHelper;
    SQLiteDatabase db;

    public DBAdapter(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    public DBAdapter open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        Log.d(AppClass.log, "DBaDAPTER.getWritableDatabase()");
        return this;
    }

    public void close() {
        DBHelper.close();
    }

    public long insertRate(double usd, double eur, String date) {
        ContentValues newval = new ContentValues();
        newval.put(KEY_DOLLAR, usd);
        newval.put(KEY_EURO, eur);
        newval.put(KEY_DATE, date);
        return db.insert(TABLE_NAME, null, newval);
    }

    public boolean deleteRowId(long rowid) {
        return db.delete(TABLE_NAME, KEY_ID + " = " + rowid, null) > 0;
    }

    public void deleteAllRowId() {
        db.execSQL("delete from " + TABLE_NAME);
    }

    public Cursor getRowOnDate(String date) {
        Log.d("myLogs", "DBaDAPTER.getRowOnDate");
        Cursor mCursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE date = ? ", new String[]{date});
        return mCursor;
    }

    public Cursor getAllRows() {
        Log.d("myLogs", "DBaDAPTER.getALL");
        //  Cursor mCursor = db.rawQuery("SELECT * FROM "+ TABLE_NAME , null);
        Cursor mCursor = db.query(true, TABLE_NAME, null, null,
                null, null, null, null, null);
        return mCursor;
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATEBASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                Log.d(AppClass.log, DATABASE_CREATE);
                db.execSQL(DATABASE_CREATE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }


}
