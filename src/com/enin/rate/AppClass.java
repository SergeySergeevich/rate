package com.enin.rate;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import java.util.Calendar;

public class AppClass extends Application {
    public static final String log = "myLogs";
    public static final String PREF = "PREF_FILE";
    private static AppClass singleton;
    private AlarmManager AlarMan;

    public static AppClass getInstance() {
        return singleton;

    }

    @Override
    public final void onCreate() {
        super.onCreate();
        singleton = this;
        setAlarm();
    }

    public void setAlarm() {
        SharedPreferences myPref = getApplicationContext().getSharedPreferences(AppClass.PREF, Activity.MODE_PRIVATE);
        int hour = myPref.getInt("Hour", 1);
        int min = myPref.getInt("Min", 0);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);
        if (calendar.get(Calendar.DAY_OF_WEEK) == 7) calendar.add(Calendar.HOUR, 24 + 24 + 24);
        else calendar.add(Calendar.HOUR, 24);
        Intent intent = new Intent("com.enin.rate.ALARM");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarMan = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        AlarMan.set(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), pendingIntent);
        Log.d(log, "setAlarm()");
    }
}
